package com.pawelbanasik.domain;

import com.pawelbanasik.service.BusinessService;

public class Organization {

	private String companyName;
	private int yearOfIncorporation;
	private String postalCode;
	private String employeeCount;
	private String slogan;
	private BusinessService businessService;
	
	
//	public Organization() {
//
//	}

	public Organization(String companyName, int yearOfIncorporation) {
		this.companyName = companyName;
		this.yearOfIncorporation = yearOfIncorporation;
	}

//	public void corporateSlogan() {
//		String slogan = "We build the ultimate driving machines";
//		System.out.println(slogan);
//	}
	
	public String corporateSlogan() {
		return slogan;
	}

	public String corporateService() {
		return businessService.offeringService(companyName);
	}
	
	@Override
	public String toString() {
		return "Organization [companyName=" + companyName + ", yearOfIncorporation=" + yearOfIncorporation
				+ ", postalCode=" + postalCode + ", employeeCount=" + employeeCount + "]";
	}


	public void setEmployeeCount(String employeeCount) {
		this.employeeCount = employeeCount;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public void setSlogan(String slogan) {
		this.slogan = slogan;
	}

	public void setBusinessService(BusinessService businessService) {
		this.businessService = businessService;
	}
	
	
}
